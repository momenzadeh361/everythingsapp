package com.example.everythingsapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class SighupActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sighup);

        Button btn_sighup_sighup = findViewById(R.id.btn_sighup_sighup);

        btn_sighup_sighup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SighupActivity.this , MainActivity.class);
                startActivity(intent);
            }
        });
    }
}