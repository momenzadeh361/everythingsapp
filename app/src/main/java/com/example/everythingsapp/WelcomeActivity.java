package com.example.everythingsapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class WelcomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        Button btn_welcome_login = findViewById(R.id.btn_welcome_login);
        Button btn_wlcome_sighup = findViewById(R.id.btn_welcome_sighup);

        btn_welcome_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentj = new Intent(WelcomeActivity.this , LoginActivity.class);
                startActivity(intentj);
            }
        });

        btn_wlcome_sighup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentd = new Intent(WelcomeActivity.this , SighupActivity.class);
                startActivity(intentd);
            }
        });
    }
}